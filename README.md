# tCSC 2019 Split, Croatia

## 2019-05-12 Sunday

### 17:30 Andrzej Nowak: Introduction to efficient computing

 - 7 Dimensions of performance
	- Top level: Logical, easy to increase performance, e.g. how to distribute data across sockets
	- Lower level: Low level instructions, e.g. vectorization (SIMD)
 - Pipeline Execution
 - Hyperthreading + Out-of-order execution -> Huge performance increase
 - Branch prediction
 - Two Intel philosophies:
	- Let hardware take care of everything
	- Let software control the hardware (Hints about branch prediction)
 - Keep programs boring and predictable -> Compiler can optimize
 - Benchmarking: "Does my code scale or not?"
	- Write modular code
	- Stable conditions/control, e.g. pin jobs to CPUs and memory
	- Emulate real conditio
	- Stable conditions/control, e.g. pin jobs to CPUs and memory
	- Emulate real conditions
 - Measurements
	- Always validate your tools!

### 18:30 Sébastien Ponce: Data storage and preservation

 - Storage pyramid
 - Reliability: Broken disks, bit write errors
 - Parallelization of storage
	- RAID 0: Striping
	- ...
	- RAIN: Software approach: Distributed systems, Redundancy
	- (!) Strpie size
	- Introducing CEPH
	- Avoid network I/O -> local computation close to the data
		- Introduciing Map/Reduce
		- HDFS: Hadoop File System
 - Ensure data consistency -> Detect data corruption / data loss
	- Checksums, e.g SUM or XOR -> doesn't detect wrong order (e.g. TCP)
	- Adler cheskum: sum + priority sum (`sum of value * position`)
	- No protection against manipulation -> cryptographic checksums
	- Checksum database (redundant !)
	- Do not only check data on user request but do regular checks (e.g. of tape storage)
 - Data safety, how to correct errors?
	- Replication: expensive, only works in combination with checksums
	- Parity, Double parity: Consequential fails problematic
	- Generalization: Erasure coding
		- Systematic: You keep the data, non systematic: You DON'T keep the original data
		- Linear transformation (with identity -> systematic)
		- Example: Reed-Solomon-Codes
	

## 2019-05-13 Monday

### 10:20 Andrzej Nowak: Software that scales with the hardware

 - How can we write software that scales with the hardware?
 - Where can we tune/optimize? -> Especially changing algorithms
 - Performance is not an Hardware issue
 - Lookup table for C++ built-ins performance
 - Accuracy: You can tell the compiler that you don't need a certain floating point precision
 - Myths vs. Reality: Not about dogma, more about tradeoffs
 - True pitfalls
 - Performance optimzation from a hardware perspective
 - 2 approaches to monitoring:
	- Hardware approach:
		- Built into CPUs
		- Quick & dirty
		- Easy
		- Hight-level view
	- Software approach:
		- Slows down the program significantly
		- Inserts probs into the code
		- Compute-intensive -> Not feasible for latency dependent tasks
		- Very sophisticated analysis possible
- Tuning
	- Iterative process
	- ...
- Recommendation
	- Avoid network file systems, FUSE mounted -> expensive
	- Local SSD is the fastet approach for data
- Optimization for throughput or latency?
- Deep CPU optimization ONLY after all other 6 approaches have been tried
- Optimize floating point operations
	- Big part of scientific software
	- Use good algorithms for standard tasks like sum or standard deviation
	- Use vectorization
	- Don't mix SSE and AVX
	- Use profiling code analysis
- Performance monitoring in hardware
	- No penalty
	- PMU: Performance monitoring unit
	- Check runtime, not theroretical LOADs and STOREs
	- perf tools on Linux
	- Histograms of most-used parts in the code
	- Cache misses, ...

### 11:10 Andrzej Nowak: Intermediate Concepts in Efficient Computing

 - TOP 500 list: Top system add GPUs, accelerators (FGPAs) -> Heterogeneous systems
 - Different levels of heterogenity: Core level,  Node level, Chip level
 - GPU computing
	- Command strted by CPU
	- Before excution, the data needs to be copied from the main memory to the GPU memory
 - Best heterogeneous programming model: Balanced/PCIe: Most benefit, but also synchronization challenge
 - FGPA: True hardware performance, useful for *online* computation
 - More and more experiments are moving to hardware
 - Keeping useful technologies from the past: Bidirectional ringbus, tiles

### 11:50 Sébastien Ponce: Key ingredients to achieve effective I/O

 - Asynchronous I/O
	- Sources of latency: HDD read, networking, ...
	- Goal: Reduce latency
	- Callback methods
	- (infinite) Message Queues + Thread pool of workers
 - I/O Optimizations
	- Goal: Speedup read/write
	- Nagle algorithm for TCP
	- Delayed ACKnowledgement
	- Potential bad combinataion: both wainting for another -> Bypass Nagle with `TCP_BYPASS`
	- (optional) Set TCP buffer size
	- Linux: Cahing via virtual file system
		- Possible data loss (e.g. during reboot) because data has not been flushed (synchronized to disk)
	- Block size is important!
		- Example: RAID 60
	- `O_DIRECT`: Bypass cache if there is no future usage of the data forseen
	- Memory-mapped files to avoid duplicates
	- Read-ahead or avoid read-ahead with kernel advice
 - Influence of data structures on I/O
	- Not only measure time complexity for algorithms, but also I/O complexity
	- There are special libraries that trade worse CPU complexity for an improved I/O complexity, e.g. STDXX for XXL data setsi (e.g. three times more data than RAM)
 - Caching
	- Where to use?
	- Garbage collection policies for full caches:
		- FIFO: Easy to implements, not that useful
		- LRU: A bit more complex, but way more useful
		- LFU: Measure frequency of access ...
		- ...
	- Writing policies:
		- Write-through: Put data first in the cache, always sync cache to disk
		- Write-back: Do NOT write directly to the backend, mark block as "dirty" -> more complex read/write
		- Writ-miss:
	- Cache parallelization
	- Cache consistency: 
 - Conclusion: Use asnchronous I/O for network, use defaults otherwise unless performance is bad.

### 16:15 Andrzej Nowak: Intermediate Concepts in Efficient Computing, part II

 - Balance programmability and performance
 - Balance data flow and compute
 - The less power you consume, the more computer you can put inside a data center
 - "Memory is king"
	- uses a lot of space and power
 - NUMA: Non-Uniform Memory Allocator
	- Memory controllers is connected to a specific CPU socket
	- Memory access becomes like network access
 - Compilers
	- Front-end: Language specifc translation into intermediate format
	- Backend: CPU specific optimizations
 - Example: PyNUFFT, putting it all together (3D brain imaging)

### 16:45 Andrzej Nowak: Data oriented design

 - Floating Point OPerations (FLOPs) will become cheaper (in energy consumption) than reading from memory -> It will be cheaper to re-calculate than reading a value from RAM
 - SIMD registers, binary (e.g AND) or unary (e.g. NOT)
 - Arrange data to enable usage of SIMD, e.g. perform 8 operations at the cost of one
 - You can automatically do that but you may want to influence it manually
 - From SSE to AVX512
 - Writing vectorized code: Easy to write (compiler hints/auto-vectorize) vs max performance (Assembler)
 - Possibilty: Smart intrinsics -> Loss of portability
 - Autovectorization: Compiler feature -> Data needs to be well-arranged/prepared
	 - Keep code simple
	 - Alwys use report
 - Compiler hints and OpenMP
 - SIMD.js, VecPy, VDT
 - Conclusion:
	- Design around data flow not control flow
	- Write simple, boring and easy-to-optmize code
	- Alignment matters (for autovectorization)
	- Reports help for debugging

## 2019-05-14 Tuesday

### 11:50 Danilo Piparo: Computational Challenges of Run III and HL-LHC

 - Different programming languages (from Radu's talk)
 - Currently used in HEP: Python wih C++ underneath


## 2019-05-15 Wednesday

### 09:00 Danilo Piparo: Concurrent Programming

 - Use almost always `auto`
 - Range-based loops
 - Goal: Reduce cost in the sense of "brain power" for the developers
 - Python comes with "Batteries included": Lots of modules to include -> Avoid re-writing software by yourself
 - No global state in C++ -> thread safe, e.g. random generator
 - Lambda functions -> Declarative programming (e.g. foreach with lambda instead of manual loop) -> Enables compiler optimization
 - `constexpr`: Calculated at compile time if concrete parameter is already known

#### Achieving correctness and good performance

 - C++ inheritence
	- `virtual` functions -> jumps (switch during runtime to find concrete implementation) -> bad performance
    - `template` -> read yourself
 - "Let the compiler help you":
	- gcc and clang
	- Performance improvements with new updates
 - Data structures and algorithms
	- STL containers: `vector`, `map`, `array`, ...
    - STL algorithms: `find`, `sort` -> stable interface

### 10:00 Sébastien Ponce: Optimizing exisitng large codebase

 - Large unkown code base, lots of unsupported code, needs to run on new hardware
 - Improve (realistic) hardware usage -> Balance costs for engineers with buying new machines
 - Measurements: Hardware statistics, software (e.g. valgrind)
 - Finding bottlenecks
 - Code modernization: Use STL and new C++ standards
 - Cleanup your code
 - Improve memory handling
	- as few memory allocations (heap) as possible
	- vectores: reserve, emplace
	- Detect memory offending code
 - Thread safety
	- Threads share memory instead of processes, price: race conditions
	- Use `const` everwhere -> compiler will complain if there are race conditions
	- If you want to use `mutables`: Use mutex, locks, ...
	- Managing state
	- Detect contentions: Only usage of 4 threads instead of 4 0 due to lock in `new`

### 11:45 Danilo Piparo: Expressing Parallelism Pragmatically

 - Understand data and task parallelism
 - Asynchronous execution
	- `std::async` from `future` library
 - Amdahl's law: Maximum speedup through parallelisation
 - Divide et Impera
 - Data and task parallelism are split artificially -> combine both of them
 - Threads: Share memory, number of threads per process doesn't need to be equal to number of threads in the CPU
 - Memory: Stack and Heap
 - `pthreads` -> `std::thread` (portable, better interface)
 - Problem: call-by-reference fails for detached threads -> original stack  is lost -> call-by-value instead (string copy), ownership wins
 - Getting back results: `std::packaged_task`
 - Thread pool model: Task queues, worker threads
 - Intel's `TBB` project provides a lot of containers, algorithms, scheduler for concurrency
 - Like OpenMP, TBB provides a parallelized for loop (not as pragma, but as a special function)


## 2019-05-16 Thursday

### 16:45 Sébastien Ponce: Practical Vectorization

 - Intro: SIMD, SSE/AVX/SVE
 - `lscpu` -> Find out supported vectorization on machine
 - Assembler code for **specific** code fragment, not the entire code base
	- Special register names for SSE, AVX2 or AVX512
	- Look for `p` (packed) and `ymm` and `zmm` (`xmm` or packed move doesn't mean much)
 - Horizontal vectorization
	- Allows Structure of Aarrays (SoA)
 - Vertival vectorization
	- Allows Array of Structures (AoS)
 - Autovectorization: Free, 100% portable
	- Still not 100% mature
	- Problems e.g. with branches (if) in loops
 - Don't write inline Assembly
 - Use intrinsics: Functions that are replaced by the compiler with efficient assembly depending on the CPU
	- Not portable
	- Vector compiler extensions to improve usability
	- Masks instead of bools (basically a vector of bools)
 - Amdahl's law for vectorization
 - Pressure on memory: "If you cannot bring the data, you cannot do cimputation", I/O limitations


